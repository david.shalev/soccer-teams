const express = require("express"),
    app = express(),
    methodOverride = require("method-override"),
    bodyParser = require("body-parser"),
    passport = require("passport"),
    localStrategy = require("passport-local"),
    mongoose = require("mongoose"),
    team = require("./models/team"),
    comment = require("./models/comment"),
    user = require("./models/user"),
    flash = require("connect-flash");

const indexRoutes = require("./routes/indexRoutes"),
    teamRoutes = require("./routes/teamRoutes"),
    commentRoutes = require("./routes/commentRoutes");

app.use(flash());
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride("_method"));
mongoose.connect("mongodb://localhost:27017/soccer", { useNewUrlParser: true, useUnifiedTopology: true });

//PASSPORT CONFIGURATION
app.use(require("express-session")({
    secret: "the secret page",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(user.authenticate()));
passport.serializeUser(user.serializeUser());
passport.deserializeUser(user.deserializeUser());
app.use(function (req, res, next) {
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
});

app.use(indexRoutes);
app.use(teamRoutes);
app.use(commentRoutes);

const PORT = process.env.PORT || 8000

app.listen(PORT, function () {
    console.log('Node app is running on port', PORT);
});


