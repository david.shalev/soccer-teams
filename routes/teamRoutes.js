const express = require("express");
const router = express.Router();
const team = require("../models/team");
const middleWare = require("../middleWare");

router.get("/teams", function (req, res) {
    team.find({}, function (err, allTeams) {
        if (err) {
            console.log(err);
        }
        else {
            res.render("team/teams", { teams: allTeams });
        }
    })
});

router.get("/teams/new",middleWare.isLoggedIn, function (req, res) {
    res.render("team/new");
});

router.post("/teams",middleWare.isLoggedIn, function (req, res) {
    let teamAuthor ={
        id:req.user._id,
        username:req.user.username
    }; 
    team.create(
        {
            name: req.body.team.team,
            img: req.body.team.img,
            description: req.body.team.description,
            author:teamAuthor
        }, function (err, newTeam) {
            if (err) {
                req.flash("error",err.message);
                res.redirect("/teams");
            }
            req.flash("success","your team has been added successfully")
            res.redirect("/teams");
        }
    )

});

router.get("/teams/:id", function (req, res) {
    team.findById(req.params.id).populate("comments").exec(function(err,foundTeam) {
        if (err) {
            console.log(err)
        } else {
            res.render("team/show", { team: foundTeam })
        }
    });
});

router.get("/teams/:id/edit",middleWare.hasTeamPremission,function(req,res){
    team.findById(req.params.id,function(err,foundTeam){
        if(err){
            req.flash("error",err.message);
            res.redirect("/teams");
        }
        else{
            res.render("team/edit",{team : foundTeam});
        }
    })
});

router.put("/teams/:id",middleWare.hasTeamPremission,function(req,res){
    team.findByIdAndUpdate(req.params.id,req.body.team,function(err,updatedTeam){
        if(err){
            req.flash("error",err.message);
            res.redirect("/index");
        }
        else{
            req.flash("success","your edition was successfully added")
            res.redirect("/teams/" + req.params.id);
        }
    })
});

router.delete("/teams/:id",middleWare.hasTeamPremission,function(req,res){
    team.findByIdAndRemove(req.params.id,function(err,destroyedTeam){
        if(err){
            req.flash("error",err.message);
            res.redirect("/teams");
        }else{
            req.flash("success","you successfully deleted your team");
            res.redirect("/teams");
        }
    })
});

module.exports = router;