const express = require("express");
const router = express.Router();
const team = require("../models/team");
const comment = require("../models/comment");
const middleWare = require("../middleWare");

router.get("/teams/:id/comments/new",middleWare.isLoggedIn,function(req,res){
    team.findById(req.params.id,function(err,foundTeam){
        if(err){
            console.log(err);
            res.redirect("/index");
        }else{
            res.render("comment/new",{team : foundTeam});
        }
    })
})

router.post("/teams/:id/comments",middleWare.isLoggedIn,function(req,res){
    team.findById(req.params.id,function(err,team){
        if(err){
            console.log(err);
            res.redirect("/index");
        }else{
            comment.create(req.body.comment ,function(err,comment){
              if(err){
                  console.log(err);
                  res.redirect("index");
              }  else{
                  comment.author.username = req.user.username;
                  comment.author.id = req.user._id;
                  comment.save();
                  team.comments.push(comment);
                  team.save();
                  req.flash("success","your comment added to the comments list successfully");
                  res.redirect("/teams/" +team._id);
              }
            }
            )}
    })
})

router.get("/teams/:id/comments/:comment_id/edit",middleWare.hasCommentPremission,function(req,res){
    comment.findById(req.params.comment_id,function(err,foundComment){
        if(err){
            res.redirect("back");
        }
        else{
            res.render("comment/edit",{team_id: req.params.id,comment: foundComment});
        }
    })
})

router.put("/teams/:id/comments/:comment_id",middleWare.hasCommentPremission,function(req,res){
   comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,function(err){
       if(err){
           res.redirect("back");
       }
       else{
           req.flash("success","you just edited your comment successfully");
           res.redirect("/teams/" + req.params.id );
       }
   })
})

router.delete("/teams/:id/comments/:comment_id",middleWare.hasCommentPremission,function(req,res){
    comment.findByIdAndRemove(req.params.comment_id,function(err){
        if(err){
            res.redirect("/");
        }else{
            req.flash("success","you just deleted your comment")
            res.redirect("back");
        }
    })
})

module.exports = router;