const express = require("express");
const router = express.Router();
const passport = require("passport");
const user = require("../models/user");

router.get("/", function (req, res) {
    res.render("index");
});

//auth routes
router.get("/register",function(req,res){
    res.render("team/register");
});

router.post("/register",function(req,res){
    const newUser = new user({username:req.body.username});
    user.register(newUser,req.body.password,function(err,user){
        if(err){
            req.flash("error",err.message);
            res.redirect("/register");
        }
        else{
            passport.authenticate("local")(req,res,function(){
                res.redirect("/teams")
            })
        }
        
    })
});

router.get("/login",function(req,res){
    res.render("team/login");
});

router.post("/login",passport.authenticate("local",
    {
      successRedirect:"/teams",
      failureRedirect:"/login"  
    }
) ,function(req,res){

});

router.get("/logout",function(req,res){
    req.logOut();
    req.flash("success","you logged out successfully");
    res.redirect("teams");
});


module.exports = router;