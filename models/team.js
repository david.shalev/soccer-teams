const mongoose = require("mongoose");

const authorSchema = new mongoose.Schema({
    id:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    username: String
});

const soccerTeamSchema = new mongoose.Schema({
    name: String,
    img: String,
    description: String,
    author: authorSchema,
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "comment"
    }]
});
module.exports = mongoose.model("team", soccerTeamSchema);