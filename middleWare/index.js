const team = require("../models/team");
const comment = require("../models/comment");
const middleWareObj = {};


middleWareObj.isLoggedIn = function(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/login");
}

 middleWareObj.hasCommentPremission = function(req,res,next){
    if(req.isAuthenticated()){
        comment.findById(req.params.comment_id,function(err,foundComment){
            if(err){
                res.redirect("back");
            }else{
                if(foundComment.author.id.equals(req.user._id)){
                    next();
                }else{
                    res.redirect("back");
                }
            }
        })
       
    }else{
        res.redirect("back");
    }
}
middleWareObj.hasTeamPremission= function(req,res,next){
    if(req.isAuthenticated()){
        team.findById(req.params.id,function(err,foundTeam){
            if(err){
                res.redirect("back");
            }
            else if(foundTeam.author.id.equals(req.user._id)){
                next();
            }
            else{
                res.redirect("back");
            }
        })   
    }
    else{
        res.redirect("back");
    }
}

module.exports =middleWareObj; 